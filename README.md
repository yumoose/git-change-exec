# Git Change Exec

## Description
When developing, you'll probably change some files. Sometimes it's really handy to be able to run some scripts, or view the files changed between two branches. Use cases include looking at files for review, running tests for changed files, and so on.

## Usage
The mergebase for the two SHAs will be taken and all changed files between the base and sha2 will be run with the given command.

```
chexec <sha1> <sha2> <command>
```

### Example

```
chexec develop master 'echo'
```

Will simply output the files that were changed between the `develop` and `master` branches.
